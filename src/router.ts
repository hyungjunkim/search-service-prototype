import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "about" */ './views/Home.vue'),
    },
    {
      path: '/product-search',
      name: 'product-search',
      component: () => import(/* webpackChunkName: "about" */ './views/product-search/ProductSearch.vue'),
    },
    {
        path: '/search-facet',
        name: 'search-facet',
        component: () => import(/* webpackChunkName: "about" */ './views/search-facet/SearchFacet.vue'),
    },
  ],
});
