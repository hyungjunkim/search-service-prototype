# blueprint

This project provides a FE guide and examples of using Search Service API.

node > 8.x is required to build.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### See
* Vue js 2.x guide  
https://vuejs.org/v2/guide/

* Vue CLI 3 guide  
https://cli.vuejs.org/guide/

* Vue router  
https://router.vuejs.org/

* Typescript Vue Starter
https://github.com/Microsoft/TypeScript-Vue-Starter

* Vue class component decorator  
https://github.com/vuejs/vue-class-component

* Vue class property decorator   
https://github.com/kaorun343/vue-property-decorator

* Vuetify (Material UI framework)  
https://vuetifyjs.com/en/getting-started/quick-start

* Vue codemirror (text editor)  
https://github.com/surmon-china/vue-codemirror